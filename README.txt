Sitemaps for everyone 

# Goals
A module to create XML sitemaps for machines 
as well as TWIG-based sitemaps for humans. 

So the first thing we need to do is extend entity.

In the beginning, the class will have an id, a url, a last updated timestamp, and a title.
We will probably extend this further later.
